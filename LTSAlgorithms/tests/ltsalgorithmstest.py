from django.test import TestCase
import numpy as np

from LTSAlgorithms.models.ltsalgorithms import *
from LTSAlgorithms.models.ltsresult import *


class LTSAlgorithmTests(TestCase):
	def testVA(self):
		mat = np.array([
		[1.,80.,27.,89.,42.],
		[1.,80.,27.,88.,37.],
		[1.,75.,25.,90.,37.],
		[1.,62.,24.,87.,28.],
		[1.,62.,22.,87.,18.],
		[1.,62.,23.,87.,18.],
		[1.,62.,24.,93.,19.],
		[1.,62.,24.,93.,20.],
		[1.,58.,23.,87.,15.],
		[1.,58.,18.,80.,14.],
		[1.,58.,18.,89.,14.],
		[1.,58.,17.,88.,13.],
		[1.,58.,18.,82.,11.],
		[1.,58.,19.,93.,12.],
		[1.,50.,18.,89., 8.],
		[1.,50.,18.,86., 7.],
		[1.,50.,19.,72., 8.],
		[1.,50.,19.,79., 8.],
		[1.,50.,20.,80., 9.],
		[1.,56.,20.,82.,15.],
		[1.,70.,20.,91.,15.]], order="F" )
		
		va = LTSAlgorithmVA(kParam=len(mat[0]), hParam=12, cycles=5000)
		va.clean_fields()
		va.save()
		va = LTSAlgorithmVA.objects.get(id=va.id)
		
		res = va.solve(mat)[0]
		va = LTSAlgorithmVA.objects.get(id=va.id)

		self.assertTrue(np.allclose(res.estimate.get_matrix()[0], np.array([-35.2095, 0.746057, 0.337795, -0.0054919]), 0.0001))
		self.assertTrue(np.alltrue(res.vector.get_matrix()[0] ==  np.array([0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0])))
		self.assertAlmostEqual(res.residualSum, 1.6371, delta=0.1)
		
	def testBSA(self):
		mat = np.array([
		[1.,80.,27.,89.,42.],
		[1.,80.,27.,88.,37.],
		[1.,75.,25.,90.,37.],
		[1.,62.,24.,87.,28.],
		[1.,62.,22.,87.,18.],
		[1.,62.,23.,87.,18.],
		[1.,62.,24.,93.,19.],
		[1.,62.,24.,93.,20.],
		[1.,58.,23.,87.,15.],
		[1.,58.,18.,80.,14.],
		[1.,58.,18.,89.,14.],
		[1.,58.,17.,88.,13.],
		[1.,58.,18.,82.,11.],
		[1.,58.,19.,93.,12.],
		[1.,50.,18.,89., 8.],
		[1.,50.,18.,86., 7.],
		[1.,50.,19.,72., 8.],
		[1.,50.,19.,79., 8.],
		[1.,50.,20.,80., 9.],
		[1.,56.,20.,82.,15.],
		[1.,70.,20.,91.,15.]], order="F" )
		
		bsa = LTSAlgorithmBSA(hParam=12)
		bsa.clean_fields()
		bsa.save()
		bsa = LTSAlgorithmBSA.objects.get(id=bsa.id)
		
		res = bsa.solve(mat)[0]
		self.assertTrue(np.allclose(res.estimate.get_matrix(), np.array([-35.2095, 0.746057, 0.337795, -0.0054919]), 0.0001))
		self.assertTrue(np.alltrue(res.vector.get_matrix() ==  np.array([0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0])))
		self.assertAlmostEqual(res.residualSum, 1.6371, delta=0.1)
		
	def testBSAM(self):
		mat = np.array([
		[1.,80.,27.,89.,42.],
		[1.,80.,27.,88.,37.],
		[1.,75.,25.,90.,37.],
		[1.,62.,24.,87.,28.],
		[1.,62.,22.,87.,18.],
		[1.,62.,23.,87.,18.],
		[1.,62.,24.,93.,19.],
		[1.,62.,24.,93.,20.],
		[1.,58.,23.,87.,15.],
		[1.,58.,18.,80.,14.],
		[1.,58.,18.,89.,14.],
		[1.,58.,17.,88.,13.],
		[1.,58.,18.,82.,11.],
		[1.,58.,19.,93.,12.],
		[1.,50.,18.,89., 8.],
		[1.,50.,18.,86., 7.],
		[1.,50.,19.,72., 8.],
		[1.,50.,19.,79., 8.],
		[1.,50.,20.,80., 9.],
		[1.,56.,20.,82.,15.],
		[1.,70.,20.,91.,15.]], order="F" )
		
		bsam = LTSAlgorithmBSAM(lowHParam=12, hParam=14)
		bsam.clean_fields()
		bsam.save()
		bsam = LTSAlgorithmBSAM.objects.get(id=bsam.id)
		
		res = bsam.solve(mat)
		self.assertTrue(np.allclose(res[0].estimate.get_matrix()[0], np.array([-35.2095, 0.746057, 0.337795, -0.0054919]), 0.0001))
		self.assertTrue(np.alltrue(res[0].vector.get_matrix()[0] ==  np.array([0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0])))
		self.assertAlmostEqual(res[0].residualSum, 1.6371, delta=0.1)
		self.assertTrue(np.allclose(res[1].estimate.get_matrix()[0], np.array([-3.73233e+01, 7.40921e-01, 3.91526e-01, 1.11345e-02]), 0.0001))
		self.assertTrue(np.alltrue(res[1].vector.get_matrix()[0] ==  np.array([0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0])))
		self.assertAlmostEqual(res[1].residualSum, 2.93239125, delta=0.1)
		
	def testBuildResult(self):
		alg = LTSAlgorithmBase()
		res = alg.buildResult(1, 1.6371, [0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0], [-35.2095, 0.746057, 0.337795, -0.0054919])
		res.save()
		res = LTSResult.objects.get(id=res.id)
		
		self.assertEqual(res.hParam, 1)
		self.assertTrue(np.allclose(res.estimate.get_matrix()[0], np.array([-35.2095, 0.746057, 0.337795, -0.0054919]), 0.0001))
		self.assertTrue(np.alltrue(res.vector.get_matrix()[0] ==  np.array([0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0])))
		self.assertAlmostEqual(res.residualSum, 1.6371, delta=0.1)
		
	def testFactoryChoices(self):
		factory = ObjectChoiceFactory()
		factory.register_class(LTSAlgorithmVA)
		factory.register_class(LTSAlgorithmBSA)
		factory.register_class(LTSAlgorithmBSAM)
		
		self.assertTrue(factory.get_choices() == [
				(LTSAlgorithmVA.__name__, 'VA algorithm'),
				(LTSAlgorithmBSA.__name__, 'BSA algorithm'),
				(LTSAlgorithmBSAM.__name__, 'BSAm algorithm')
		])

	
		
		