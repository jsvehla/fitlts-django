from django.test import TestCase

from LTSAlgorithms.models import LTSTransNone
from LTSAlgorithms.models import LTSTransMultiply
from LTSAlgorithms.models import LTSTransPower
from LTSAlgorithms.models import LTSTransExclude


class TransformationTests(TestCase):
	def testNone(self):
		nonetr = LTSTransNone()
		
		array = nonetr.transform(range(0,10))
		self.assertListEqual(array, range(0,10))
		
	def testMultiplyPositive(self):
		trans = LTSTransMultiply()
		trans.parameter = 2
		
		array = trans.transform(range(0,10))
		self.assertListEqual(array, [0,2,4,6,8,10,12,14,16,18])
		
	def testMultiplyNegative(self):
		trans = LTSTransMultiply()
		trans.parameter = -1
		
		array = trans.transform(range(0,10))
		self.assertListEqual(array, range(0, -10, -1))
		
	def testMultiplyPower(self):
		trans = LTSTransPower()
		trans.parameter = 2
		
		array = trans.transform(range(0,10))
		self.assertListEqual(array, [0,1,4,9,16,25,36,49,64,81])
		
	def testMultiplyExclude(self):
		trans = LTSTransExclude()
		array = trans.transform(range(1,10))
				
		self.assertTrue(array == None)
		