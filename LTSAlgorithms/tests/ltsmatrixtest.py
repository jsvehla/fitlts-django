from django.test import TestCase
import numpy as np

from LTSAlgorithms.models import *


class MatrixTests(TestCase):

	def testMatrixTrans(self):
		m = LTSMatrix()
		m.save()
		m.fillArray([
		[1,2,3,2],
		[1,2,3,0],
		[3,2,1,4],
		[3,2,1,5]])
		m.save()
			
		m.add_transformation(LTSTransNone())
		m.add_transformation(LTSTransExclude())
		m.add_transformation(LTSTransPower(parameter=2))
		m.add_transformation(LTSTransMultiply(parameter=4))
		
		m = m.get_matrix_trans()
		self.assertTrue(np.alltrue(m[0] == np.array([1, 9,  8])))
		self.assertTrue(np.alltrue(m[1] == np.array([1, 9,  0])))
		self.assertTrue(np.alltrue(m[2] == np.array([3, 1, 16])))
		self.assertTrue(np.alltrue(m[3] == np.array([3, 1, 20])))

	def testAddRow(self):
		m = LTSMatrix()
		m.save() #set id
			
		m.addRow([1,2,3])
		m.addRow([1,2,3])
		m.addRow([3,2,1])
		m.save()
		mat = LTSMatrix.objects.get(id=m.id)
	
		self.assertTrue(mat.x_dim == 3)
		self.assertTrue(mat.y_dim == 3)
		self.assertTrue(mat.ltsrow_set.count() == 3)
	
	def testFillMatrix(self):
		m = LTSMatrix()
		m.save() #set id
			
		m.fillArray([[1,2,3], [1,2,3], [3,2,1]])
		m.save()
		mat = LTSMatrix.objects.get(id=m.id)
	
		self.assertTrue(mat.x_dim == 3)
		self.assertTrue(mat.y_dim == 3)
		self.assertTrue(mat.ltsrow_set.count() == 3)
		
		m = mat.get_matrix()
		self.assertTrue(np.alltrue(m[0] == np.array([1,2,3])))
		self.assertTrue(np.alltrue(m[1] == np.array([1,2,3])))
		self.assertTrue(np.alltrue(m[2] == np.array([3,2,1])))
		
		
	def testGetMatrixOrdered(self):
		m = LTSMatrix()
		m.save()
		m.addRow([3,2,1])
		m.addRow([1,2,3])
		m.save()
		m = LTSMatrix.objects.get(id=m.id)
		
		mat = m.get_matrix()
		self.assertTrue(m.ltsrow_set.count() == 2)
		self.assertTrue(np.alltrue(mat[1] == np.array([1,2,3])))
		self.assertTrue(np.alltrue(mat[0] == np.array([3,2,1])))