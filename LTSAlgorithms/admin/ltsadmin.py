from django.contrib import admin
from django.core import urlresolvers
from django.contrib.contenttypes.models import ContentType

from LTSAlgorithms.models import LTSMatrix
from LTSAlgorithms.models import LTSRow
from LTSAlgorithms.models import LTSAlgorithmBase
from LTSAlgorithms.models import LTSAlgorithmVA
from LTSAlgorithms.models import LTSAlgorithmBSA
from LTSAlgorithms.models import LTSAlgorithmBSAM
from LTSAlgorithms.models import LTSResult
from LTSAlgorithms.models import LTSTransBase
from LTSAlgorithms.models import LTSTransNone
from LTSAlgorithms.models import LTSTransMultiply
from LTSAlgorithms.models import LTSTransPower
from LTSAlgorithms.models import LTSTransExclude
from LTSAlgorithms.models import LTSTransLog

class CustomAdminBase(admin.ModelAdmin):
	"""Base for all admins for FITLTS.
	Overloads first column and removes add/delete permissions.
	"""	
	list_display = ('class_name_id',)
	
	def class_name_id(self, obj):
		return "%s  id:%s" % (str(obj.__class__.__name__), obj.id)
		
	def has_add_permission(self, request):
		return False
	
	def has_delete_permission(self, request, obj=None):
		return False
	
	def get_id_text(self, obj):
		id = obj.id
		content_type = ContentType.objects.get_for_model(obj.__class__)
		url = urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(id,))
		text = unicode(id)
		return u"<a href='%s'>%s</a>" % (url, text)

class HiddenModelAdmin(CustomAdminBase):
	"""Hides models from admin view using custom added 'view' permission"
	"""
	def get_model_perms(self, *args, **kwargs):
		perms = admin.ModelAdmin.get_model_perms(self, *args, **kwargs)
		perms['list_hide'] = True
		return perms
		
class SubclassedAdmin(CustomAdminBase):
	"""Allows multiple subclasses into same view. Redirects to the correct view."""
	def class_name_id(self, obj):
		content_type = ContentType.objects.get_for_model(obj.__class__)
		url = urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(obj.id,))
		text = "%s  id:%s" % (str(obj.__class__.__name__), obj.id)
		return u"<a href='%s'>%s</a>" % (url, text)
	class_name_id.allow_tags = True
		
class LTSRowInline(admin.StackedInline):
	"""Inline view for LTSRow in LTSMatrix view."""
	model = LTSRow
	extra = 0
	fieldsets = (
		(None, {
		'fields': ()
		}),
	)
	verbose_name = ""
	verbose_name_plural = "Associated rows"
	
	def has_add_permission(self, request):
		return False
	
	def has_delete_permission(self, request, obj=None):
		return False
	
	def has_change_permission(self, request, obj=None):
		return True
	
class LTSMatrixAdmin(CustomAdminBase):
	list_display = CustomAdminBase.list_display + ('x_dim', 'y_dim')
	readonly_fields = ('x_dim','y_dim')
	
	inlines = [LTSRowInline,]
	
class LTSRowAdmin(CustomAdminBase):
	list_display = CustomAdminBase.list_display + ( 'related_matrix_id', '_row_num' )
	readonly_fields = ('_matrix','_row_num')
	list_filter = ('_matrix',)	
	fieldsets = (
		(None, {
		'fields': ('_row_num', )
		}),
	)
		
	def related_matrix_id(self, obj):
		return self.get_id_text(obj._matrix)
	related_matrix_id.allow_tags = True
	
class LTSAlgorithmBaseAdmin(SubclassedAdmin):
	list_display = CustomAdminBase.list_display + ( 'related_task_id', 'hParam',)
	list_filter = ('ltstask', )
	
	def related_task_id(self, obj):
		return self.get_id_text(obj.ltstask)
	related_task_id.allow_tags = True	

class LTSAlgorithmVAAdmin(HiddenModelAdmin, LTSAlgorithmBaseAdmin):
	pass
		
class LTSAlgorithmBSAAdmin(HiddenModelAdmin, LTSAlgorithmBaseAdmin):
	pass
	
class LTSAlgorithmBSAMAdmin(HiddenModelAdmin, LTSAlgorithmBaseAdmin):
	pass
	
class LTSResultAdmin(CustomAdminBase):
	list_display = CustomAdminBase.list_display + ( 'for_task_id', 'hParam', 'residualSum', 'estimate_matrix_id', 'vector_matrix_id' )
	readonly_fields = ('for_task','hParam', 'residualSum', 'estimate', 'vector')
	list_filter = ('for_task',)
	
	def for_task_id(self, obj):
		return self.get_id_text(obj.for_task)
	for_task_id.allow_tags = True
	
	def estimate_matrix_id(self, obj):
		return self.get_id_text(obj.estimate)
	estimate_matrix_id.allow_tags = True
	
	def vector_matrix_id(self, obj):
		return self.get_id_text(obj.vector)
	vector_matrix_id.allow_tags = True

class LTSTransBaseAdmin(SubclassedAdmin):
	list_display = SubclassedAdmin.list_display + ( 'related_matrix_id', '_col_num')
	list_filter = ('_matrix',)
	readonly_fields = ('_matrix','_col_num')
	
	def related_matrix_id(self, obj):
		return self.get_id_text(obj._matrix)
	related_matrix_id.allow_tags = True
		
class LTSTransNoneAdmin(HiddenModelAdmin, LTSTransBaseAdmin):
	pass
	
class LTSTransMultiplyAdmin(HiddenModelAdmin, LTSTransBaseAdmin):
	list_display = LTSTransBaseAdmin.list_display + ( 'parameter', )
	
class LTSTransPowerAdmin(HiddenModelAdmin, LTSTransBaseAdmin):
	list_display = LTSTransBaseAdmin.list_display + ( 'parameter', )
	
class LTSTransExcludeAdmin(HiddenModelAdmin, LTSTransBaseAdmin):
	pass
	
class LTSTransLogAdmin(HiddenModelAdmin, LTSTransBaseAdmin):
	pass

admin.site.register(LTSMatrix, LTSMatrixAdmin)
admin.site.register(LTSRow, LTSRowAdmin)
admin.site.register(LTSAlgorithmVA, LTSAlgorithmVAAdmin)
admin.site.register(LTSAlgorithmBSA, LTSAlgorithmBSAAdmin)
admin.site.register(LTSAlgorithmBSAM, LTSAlgorithmBSAMAdmin)
admin.site.register(LTSAlgorithmBase, LTSAlgorithmBaseAdmin)
admin.site.register(LTSResult, LTSResultAdmin)
admin.site.register(LTSTransBase, LTSTransBaseAdmin)
admin.site.register(LTSTransNone, LTSTransNoneAdmin)
admin.site.register(LTSTransMultiply, LTSTransMultiplyAdmin)
admin.site.register(LTSTransPower, LTSTransPowerAdmin)
admin.site.register(LTSTransExclude, LTSTransExcludeAdmin)
admin.site.register(LTSTransLog, LTSTransExcludeAdmin)