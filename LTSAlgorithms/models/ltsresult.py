from django.db import models

from ltsmatrix import LTSMatrix

class LTSResult(models.Model):
	"""Result structure. can be associated with task"""
	estimate = models.OneToOneField(LTSMatrix, related_name="result_estimate")
	vector = models.OneToOneField(LTSMatrix, related_name="result_vector")
	residualSum = models.FloatField()
	hParam = models.IntegerField()
	
	for_task = models.ForeignKey('LTSTasks.LTSTask', null=True, blank=True, related_name='result_set')
	
	def getVector(self):
		"""Returns a matrix of data points.
		
		returns: LTSMatrix
		"""
		return self.vector.get_matrix()
	
	def getEstimate(self):
		"""Returns a matrix of estimate.
		
		returns: LTSMatrix
		"""
		return self.estimate.get_matrix()
		
	def getResidualSum(self):
		"""Returns residual sum
		
		returns: Number
		"""
		return self.residualSum
	
	def __unicode__(self):
		return "LTSResult estimate:[" + unicode(self.estimate) + "] vector:[" + unicode(self.vector) + "] sum:" + unicode(self.residualSum)
	
	class Meta:
		app_label = 'LTSAlgorithms'
		verbose_name = 'LTSResult'
		verbose_name_plural = 'LTSResults'
		ordering = ['for_task', 'hParam']