import math
from django.db import models
from django.contrib.contenttypes.models import ContentType

from LTSAlgorithms.views import SubclassingManager
from ltsmatrix import LTSMatrix
from objectfactory import ObjectChoiceFactory

class LTSTransBase(models.Model):
	"""Base for transformations."""
	content_type = models.ForeignKey(ContentType,editable=False,null=True)
	objects = SubclassingManager()
	_matrix = models.ForeignKey(LTSMatrix, related_name='ltstransformation_set')
	_col_num = models.IntegerField()
	
	def transform(self, array):
		"""Transform matrix row.
		
		array	-- row array
		
		returns: transformed List
		"""
		raise NotImplementedError
			
	@staticmethod
	def get_long_name():
		"""Returns name of transformation for UI"""
		raise NotImplementedError
			
	def __unicode__(self):
		return "LTSTransBase matrix:[" + unicode(self._matrix) + "]  column:" + unicode(self._col_num)
	
	def accept(self, visitor):
		visitor.visit(self)	
	
	class Meta:
		verbose_name = 'LTSTransformation'
		verbose_name_plural = 'LTSTransformations'
		app_label = 'LTSAlgorithms'
		ordering = ['_matrix__id','_col_num']
	
	def save(self, *args, **kwargs):
		"""
		Saves subclasses with content type
		"""
	
		if(not self.content_type):
			self.content_type = ContentType.objects.get_for_model(self.__class__)
			super(LTSTransBase, self).save(*args, **kwargs)

	def as_leaf_class(self):
		"""
		returns child as its real type
		"""
		content_type = self.content_type
		model = content_type.model_class()
		if (model == LTSTransBase):
			return self
		return model.objects.get(id=self.id)

class LTSTransNone(LTSTransBase):
	"""No transformation"""
	def transform(self, array):
		return array
			
	@staticmethod
	def get_long_name():
		return 'None'
	
	def __unicode__(self):
		return "LTSTransNone matrix:[" + unicode(self._matrix) + "]  column:" + unicode(self._col_num)
	
	class Meta:
		verbose_name = 'LTSTransNone'
		verbose_name_plural = 'LTSTransNone'
		app_label = 'LTSAlgorithms'

class LTSTransMultiply(LTSTransBase):
	"""Mulitplies row by number"""
	parameter = models.IntegerField()
	
	def transform(self, array):
		return [x * self.parameter for x in array]
			
	@staticmethod
	def get_long_name():
		return 'Multiply'
	
	def __unicode__(self):
		return "LTSTransMultiply matrix:[" + unicode(self._matrix) + "]  column:" + unicode(self._col_num) + " parameter:" + unicode(self.parameter)
	
	class Meta:
		verbose_name = 'LTSTransMultiply'
		verbose_name_plural = 'LTSTransMultiply'
		app_label = 'LTSAlgorithms'
		
class LTSTransPower(LTSTransBase):
	"""Transform row using power function"""
	parameter = models.PositiveIntegerField()
	
	def transform(self, array):
		return [math.pow(x, self.parameter) for x in array]
			
	@staticmethod
	def get_long_name():
		return 'Power'
			
	def __unicode__(self):
		return "LTSTransPower matrix:[" + unicode(self._matrix) + "]  column:" + unicode(self._col_num) + " parameter:" + unicode(self.parameter)
	
	class Meta:
		verbose_name = 'LTSTransPower'
		verbose_name_plural = 'LTSTransPower'
		app_label = 'LTSAlgorithms'

class LTSTransLog(LTSTransBase):
	"""Logarithm transformation."""
	base = models.PositiveIntegerField()
	
	def transform(self, array):
		return [math.log(x, self.base) for x in array]
			
	@staticmethod
	def get_long_name():
		return 'Logarithm'
	
	def __unicode__(self):
		return "LTSTransLog matrix:[" + unicode(self._matrix) + "]  column:" + unicode(self._col_num) + " parameter:" + unicode(self.base)
	
	class Meta:
		verbose_name = 'LTSTransLog'
		verbose_name_plural = 'LTSTransLog'
		app_label = 'LTSAlgorithms'
		
class LTSTransExclude(LTSTransBase):
	"""Excludes column from matrix. """
	def transform(self, array):
		"""returns none."""
		return None
		
	@staticmethod
	def get_long_name():
		return 'Exclude'
			
	def __unicode__(self):
		return "LTSTransExclude matrix:[" + unicode(self._matrix) + "]  column:" + unicode(self._col_num)
		
	class Meta:
		verbose_name = 'LTSTransExclude'
		verbose_name_plural = 'LTSTransExclude'
		app_label = 'LTSAlgorithms'