from django.db import models
import numpy as np
import base64

class Base64Array(models.TextField):
	"""Custom fieald to save numpy array into database as base64 string"""
	def contribute_to_class(self, cls, name):
		"""Add new raw field into class
		
		cls		-- class
		name	-- field name
		"""
		if self.db_column is None:
			self.db_column = name
		self.field_name = name + '_base64'
		super(Base64Array, self).contribute_to_class(cls, self.field_name)
		setattr(cls, name, property(self.get_data, self.set_data))

	def get_data(self, obj):
		"""Decodes data from field.
		
		obj -- object with field
		
		returns: numpy array
		"""		
		tmp = base64.decodestring(getattr(obj, self.field_name))
		return np.frombuffer(tmp, dtype = np.float64)

	def set_data(self, obj, data):
		"""Sets data to field.
		
		obj		-- object with field
		data	-- numpy array
		"""		
		tmp = data.astype(np.float64)
		tmp = base64.b64encode(tmp)
		setattr(obj, self. field_name, tmp)