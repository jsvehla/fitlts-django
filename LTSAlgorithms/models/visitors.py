import visitor_utils as dispatch

from LTSAlgorithms.models import LTSAlgorithmVA
from LTSAlgorithms.models import LTSAlgorithmBSA
from LTSAlgorithms.models import LTSAlgorithmBSAM
from LTSAlgorithms.models import LTSTransExclude
from LTSAlgorithms.models import LTSTransNone
from LTSAlgorithms.models import LTSTransLog
from LTSAlgorithms.models import LTSTransPower
from LTSAlgorithms.models import LTSTransMultiply

class ParamVisitor(object):
	"""Reads parameters for given algorithm and returns the dict for UI."""
	def __init__(self):
		self.params = []
		
	@dispatch.on('node')
	def visit(self, node):
		pass
		
	@dispatch.when(LTSAlgorithmVA)
	def visit(self, node):
		self.params.append({'name': 'H', 'value' : node.hParam}) 
		self.params.append({'name': 'K', 'value' : node.kParam})
		self.params.append({'name': 'cycles', 'value' : node.cycles})
		
	@dispatch.when(LTSAlgorithmBSA)
	def visit(self, node):
		self.params.append({'name': 'H', 'value' : node.hParam}) 
		
		
	@dispatch.when(LTSAlgorithmBSAM)
	def visit(self, node):
		self.params.append({'name': 'low H', 'value' : node.lowHParam})
		self.params.append({'name': 'H', 'value' : node.hParam})
		
class TransParamVisitor(object):
	"""Reads parameters for given transformation and returns the dict for UI."""
	def __init__(self):
		self.params = []
		
	@dispatch.on('node')
	def visit(self, node):
		pass
		
	@dispatch.when(LTSTransExclude)
	def visit(self, node):
		pass
		
	@dispatch.when(LTSTransNone)
	def visit(self, node):
		pass
		
	@dispatch.when(LTSTransPower)
	def visit(self, node):
		self.params.append({'name': 'parameter', 'value' : node.parameter}) 
	
	@dispatch.when(LTSTransMultiply)
	def visit(self, node):
		self.params.append({'name': 'parameter', 'value' : node.parameter}) 
		
	@dispatch.when(LTSTransLog)
	def visit(self, node):
		self.params.append({'name': 'base', 'value' : node.base}) 

		
class TransFormParamVisitor(object):
	"""Reads parameters for given transformation and returns the value to be used in transformation form."""
	@dispatch.on('node')
	def visit(self, node):
		pass
		
	@dispatch.when(LTSTransExclude)
	def visit(self, node):
		self.parameter = None
		
	@dispatch.when(LTSTransNone)
	def visit(self, node):
		self.parameter = None
		
	@dispatch.when(LTSTransPower)
	def visit(self, node):
		self.parameter = node.parameter
	
	@dispatch.when(LTSTransMultiply)
	def visit(self, node):
		self.parameter = node.parameter
		
	@dispatch.when(LTSTransLog)
	def visit(self, node):
		self.parameter = node.base