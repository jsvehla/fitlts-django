from django.db import models
import numpy as np

from fields import Base64Array

class LTSMatrix(models.Model):
	"""Matrix class. Uses LTSRow to save data."""
	x_dim = models.IntegerField(default=0, blank=False)
	y_dim = models.IntegerField(default=0, blank=False)

	def addRow(self, array):
		"""Adds new row from array to the matrix.
		
		array	-- List of data
		"""
		if self.ltsrow_set.count() == 0:
			self.x_dim = len(array)
			self.y_dim = 0
		if len(array) != self.x_dim:
			raise ValueError("Arrays must have same number of dimensions")
			
		tmp = LTSRow()
		tmp.data = np.array(array, order="F")
		tmp._row_num = self.y_dim
		self.ltsrow_set.add(tmp)
		tmp.save()
		
		self.y_dim += 1
		
	def add_transformation(self, trans):
		"""Adds transformation to the matrix.
		
		trans	-- LTSTransBase subtype
		"""
		count = self.ltstransformation_set.count()
		if count > self.x_dim:
			raise ValueError("Can't add more transformations that columns")
		trans._col_num = count
		trans._matrix = self
		trans.save()
		
	def get_matrix(self):
		"""Return untransformed matrix.
		
		returns: List
		"""
		if self.ltsrow_set.count() == 0:
			return np.array([])
		
		rows = self.ltsrow_set.all()
		self.matrix = np.array([rows[0].data])
		for i in range(1, self.y_dim):
			self.matrix = np.append(self.matrix, [rows[i].data], axis=0)
		return self.matrix
		
	def get_matrix_trans(self):
		"""Returns transformed matrix.
		
		returns: List
		"""
		orig_mat = self.get_matrix().transpose()
		new_mat = []
		
		trans = self.ltstransformation_set.all()
		for tr in trans:
			arr = tr.transform(orig_mat[tr._col_num])
			if(arr != None):
				new_mat.append(arr)
		
		# incorrect transformations - list empty or incomplete
		if not new_mat or len(trans) != len(orig_mat):
			return orig_mat.transpose()	# return as if all transformations were none
		
		# transposing creates Fortran order
		return np.array(new_mat).transpose()
		
	def fillArray(self, array):
		"""Fills matrix from 2D list.
		
		array -- List
		"""
		
		for arr in array:
			self.addRow(arr)
		
	def __unicode__(self):
		return ("LTSMatrix xdim:" + unicode(self.x_dim) + " ydim:" + unicode(self.y_dim))

	class Meta:
		app_label = 'LTSAlgorithms'
		verbose_name = 'LTSmatrix'
		verbose_name_plural = 'LTSMatrices'

class LTSRow(models.Model):	
	data =  Base64Array()
	_matrix = models.ForeignKey(LTSMatrix)
	_row_num = models.IntegerField()

	def __len__(self):
		return len(self.data)
		
	def __unicode__(self):
		return ("LTSRow matrix:" + unicode(self._matrix.id) + " rownum:" +  unicode(self._row_num) + " data:" + str(self.data))
	
	class Meta:
		app_label = 'LTSAlgorithms'
		verbose_name = 'LTSRow'
		verbose_name_plural = 'LTSRows'
		ordering = ['_matrix__id','_row_num']

	

	