from django.db import models
import numpy as np
from django.contrib.contenttypes.models import ContentType

from LTSAlgorithms.views import SubclassingManager
from LTSAlgorithms.wrapper import fitltswrapper as fw
from ltsresult import LTSResult
from ltsresult import LTSMatrix
from objectfactory import ObjectChoiceFactory


class LTSAlgorithmBase(models.Model):
	"""Base for algorithms."""
	content_type = models.ForeignKey(ContentType,editable=False,null=True)
	objects = SubclassingManager()
	hParam = models.IntegerField(blank = False, verbose_name="Parameter H")
	
	@staticmethod		
	def get_long_name():
		"""Returns a long name of the class. Used to name class in UI."""
		raise NotImplementedError

	def solve(self, matrix):
		"""Solves the matrix.
		
		matrix	-- matrix
		returns: LTSResult array
		"""
		raise NotImplementedError
		
	def prepare_matrix(self, matrix):
		"""Prepares array to be passed to SWIG
		returns: nympyarray
		"""
		# force copy array to avoid SWIG error when array doesn't own it's own memory
		return np.array(matrix, dtype=np.float64, order="F")
		
	def buildResult(self, hParam, residualSum, vector, estimate):
		"""Creates result from computed data.
		
		hParam		-- parameter h
		residualSum	-- sum of residuals
		vector		-- binary vector
		estimate	-- lts estimate
		
		returns: LTSResult
		"""
		res = LTSResult()

		tmp = LTSMatrix.objects.create()
		tmp.addRow(vector)
		tmp.save()
		res.vector = tmp
		
		tmp = LTSMatrix.objects.create()
		tmp.addRow(estimate)
		tmp.save()
		res.estimate = tmp
		
		res.residualSum = residualSum
		res.hParam = hParam
		res.save()
		
		return res
	
	def accept(self, visitor):
		visitor.visit(self)	
	
	class Meta:
		verbose_name = 'LTSAlgorithm'
		verbose_name_plural = 'LTSAlgorithms'
		app_label = 'LTSAlgorithms'
	
	def save(self, *args, **kwargs):
		"""
		Saves subclasses with content type
		"""	
		if(not self.content_type):
			self.content_type = ContentType.objects.get_for_model(self.__class__)
			super(LTSAlgorithmBase, self).save(*args, **kwargs)

	def as_leaf_class(self):
		"""
		returns child as its real type
		"""
		content_type = self.content_type
		model = content_type.model_class()
		if (model == LTSAlgorithmBase):
			return self
		return model.objects.get(id=self.id)
	
class LTSAlgorithmVA(LTSAlgorithmBase):
	"""Implementation of VA method."""
	objects = SubclassingManager()
	kParam = models.IntegerField(blank = False, null=False, verbose_name="Parameter K")
	cycles = models.IntegerField(blank = False, null=False, verbose_name="Parameter Cycles")
	
	@staticmethod	
	def get_long_name():
		return 'VA algorithm'
	
	def solve(self, matrix):
		ex = fw.FITLTSWrapper( self.prepare_matrix( matrix ))
		ex.solveVA(self.cycles, self.kParam, self.hParam )
				
		return [self.buildResult(self.hParam, ex.getResultResiduals()[0], ex.getResultVectors()[0], ex.getResultEstimates()[0])]
	
	def __unicode__(self):
		return ("LTSAlgorithmVA h:" + unicode(self.hParam) + " k:" + unicode(self.kParam) + " cycles:" + unicode(self.cycles))
	
	class Meta:
		verbose_name = 'LTSAlgorithmVA'
		verbose_name_plural = 'LTSAlgorithmVA'
		app_label = 'LTSAlgorithms'
	
class LTSAlgorithmBSA(LTSAlgorithmBase):
	"""Implementation of BSA method"""
	objects = SubclassingManager()
	
	@staticmethod	
	def get_long_name():
		return 'BSA algorithm'
	
	def solve(self, matrix):
		ex = fw.FITLTSWrapper( self.prepare_matrix(matrix ))
		ex.solveBSA(self.hParam )
				
		return [self.buildResult(self.hParam, ex.getResultResiduals()[0], ex.getResultVectors()[0], ex.getResultEstimates()[0])]
		
	def __unicode__(self):
		return ("LTSAlgorithmBSA h:" + unicode(self.hParam))
		
	class Meta:
		verbose_name = 'LTSAlgorithmBSA'
		verbose_name_plural = 'LTSAlgorithmBSA'
		app_label = 'LTSAlgorithms'
	
class LTSAlgorithmBSAM(LTSAlgorithmBase):
	"""Implementation of BSAM method"""
	objects = SubclassingManager()
	lowHParam = models.IntegerField(blank = False, null=False, verbose_name="Parameter Low H")
	
	@staticmethod
	def get_long_name():
		return 'BSAm algorithm'
	
	def solve(self, matrix):
		ex = fw.FITLTSWrapper( self.prepare_matrix(matrix ))
		ex.solveBSAM(self.lowHParam, self.hParam )
		
		residuals = ex.getResultResiduals()
		vectors = ex.getResultVectors()
		estimates = ex.getResultEstimates()
		results = []
				
		for i in range(0, len(residuals)):
			results.append(self.buildResult(self.lowHParam + i, residuals[i], vectors[i], estimates[i]))
				
		return results
	
	def __unicode__(self):
		return ("LTSAlgorithmBSAM lh:" + unicode(self.lowHParam) + " h:" + unicode(self.hParam))
	
	class Meta:
		verbose_name = 'LTSAlgorithmBSAM'
		verbose_name_plural = 'LTSAlgorithmBSAM'
		app_label = 'LTSAlgorithms'