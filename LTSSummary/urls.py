from django.conf.urls import patterns, url
from django.views.generic import RedirectView

from LTSSummary import views

urlpatterns = patterns('',
	url(r'^(?P<group_id>[A-Za-z0-9_-]+)$', views.summary, name='summary'),
	url(r'^(?P<group_id>[A-Za-z0-9_-]+)/pdf$', views.summary_pdf, name='pdf')
)