from django.http import Http404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from LTSTasks.models import LTSTaskGroup
from LTSTasks.models import LTSTask
from LTSSummary import models as m
from LTSTasks.wizard import WizardEdit

def summary(request, group_id):
	if "_duplicate" in request.POST:
		return duplicate(request, group_id)
	if "_cancel" in request.POST:
		task_id = int(request.POST['_cancel_task'])
		return cancel_task(request, group_id, task_id)
	return render_summary(request, group_id)
		
def render_summary(request, group_id):
	try:
		group = LTSTaskGroup.objects.get(uuid=group_id)
		renderer = m.RendererCompare(group)
		return renderer.render_response(request)
	except LTSTaskGroup.DoesNotExist:
		raise Http404
	
def summary_pdf(request, group_id):
	try:
		group = LTSTaskGroup.objects.get(uuid=group_id)
		renderer = m.RendererPDF(group)
		return renderer.render_response(request)
	except LTSTaskGroup.DoesNotExist:
		raise Http404
		
def cancel_task(request, group_id, task_id):
	try:
		group = LTSTaskGroup.objects.get(uuid=group_id)
		task = group.ltstask_set.get(id=task_id)
		task.cancel()	
		return HttpResponseRedirect(reverse('ltssummary:summary', args=(group_id,)))
	except (LTSTaskGroup.DoesNotExist, LTSTask.DoesNotExist):
		raise Http404
		
def duplicate(request, group_id):
	try:
		group = LTSTaskGroup.objects.get(uuid=group_id)
		WizardEdit().prepare_data(request, group)
		return HttpResponseRedirect(reverse('ltstasks:wizard_edit', args=('start',)))		
		
	except LTSTaskGroup.DoesNotExist:
		raise Http404
	