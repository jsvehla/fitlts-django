from django import http
from django.shortcuts import render
from django.template.loader import get_template
from django.template import Context
import xhtml2pdf.pisa as pisa
import cStringIO as StringIO
import cgi
import numpy as np

from LTSAlgorithms.models import ParamVisitor

class RendererBase(object):
	"""Base class for renderers."""
	def __init__(self, task_group):
		self.group = task_group
	
	def strvector(self, matrix):
		"""Transforms matrix into readable double vector."""
		m = matrix.get_matrix_trans()
		return "(" + ", ".join( str(x) for x in np.nditer(m)) + ")"
	
	def strvector_bool(self, matrix):
		"""Transforms matrix into readable boolean vector."""
		m = matrix.get_matrix_trans()
		return "(" + ", ".join( str(int(x)) for x in np.nditer(m)) + ")"
	
	def base_context(self, request):
		"""Provides the context dictionary created from the taskgrou data for the template."""
		tasks = self.group.ltstask_set.all()
		tasks_data = []
		matrix = tasks[0].matrix.get_matrix_trans()
		
		local_id = 0
		for t in tasks:
			tmp = {}
			tmp['status'] = t.state.get_long_name()
			tmp['id'] = t.id
			local_id += 1
			tmp['local_id'] = local_id
			tmp['algorithm'] = t.algorithm.as_leaf_class().get_long_name()
			pvis = ParamVisitor()
			t.algorithm.as_leaf_class().accept(pvis)
			tmp['params'] = pvis.params
			tmp['date_created'] = t.date_created.isoformat(' ')
			
			tmp['results'] = []
			related_results = t.result_set.all()
			if(related_results):
				tmp['running_time'] = (t.date_finished - t.date_started).total_seconds()
				for r in related_results:
					ttmp =	{}
					ttmp['H'] = r.hParam
					ttmp['residual_sum'] = r.residualSum
					ttmp['vector'] = self.strvector_bool(r.vector)
					ttmp['estimate'] = self.strvector(r.estimate)
					tmp['results'].append(ttmp)
					
			tasks_data.append(tmp)
			
		context = {'matrix': matrix, 'tasks' : tasks_data, 'job_id' : self.group.uuid}
		return context
	
	def render_response(self, request):
		raise NotImplementedError
		
class RendererSimple(RendererBase):
	"""Renders simple template."""
	def render_response(self, request):
		return render(request, 'LTSSummary/summary_simple.html', self.base_context(request))
		
class RendererCompare(RendererBase):
	"""Renders template with compare table."""
	def render_response(self, request):
		return render(request, 'LTSSummary/summary_compare.html', self.base_context(request))
		
class RendererPDF(RendererBase):
	"""Renders PDF from template."""
	
	def render_pdf(self, template_src, context_dict):
		"""Renders tempate and converts it to the pdf."""
		template = get_template(template_src)
		context = Context(context_dict)
		html  = template.render(context)
		result = StringIO.StringIO()
		pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
		if not pdf.err:
			return http.HttpResponse(result.getvalue(), mimetype='application/pdf')
		return http.HttpResponse("Error Generating PDF! %s" % cgi.escape(html))
	
	def render_response(self, request):
		 return self.render_pdf('LTSSummary/summary_pdf.html', self.base_context(request))