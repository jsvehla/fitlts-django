MAX_ALGORITHMS_PER_GROUP = 5

ALGORITHM_CLS = (
	("LTSAlgorithms.models.LTSAlgorithmVA", "LTSTasks.forms.AlgorithmVADetailsForm"),
	("LTSAlgorithms.models.LTSAlgorithmBSA", "LTSTasks.forms.AlgorithmBSADetailsForm"),
	("LTSAlgorithms.models.LTSAlgorithmBSAM", "LTSTasks.forms.AlgorithmBSAMDetailsForm"),
)

TRANSFORMATION_CLS = (
	"LTSAlgorithms.models.LTSTransNone",
	"LTSAlgorithms.models.LTSTransExclude",
	"LTSAlgorithms.models.LTSTransMultiply",
	"LTSAlgorithms.models.LTSTransPower",
	"LTSAlgorithms.models.LTSTransLog",
)