#BROKER_BACKEND = "djkombu.transport.DatabaseTransport"

BROKER_URL = "amqp://guest:guest@localhost:5672//"
CELERY_RESULT_BACKEND = "amqp"
CELERY_TASK_RESULT_EXPIRES = None

TEST_RUNNER = 'djcelery.contrib.test_runner.CeleryTestSuiteRunner'
CELERY_IMPORTS = ("QueueCelery.models.queuetasks")