from django.shortcuts import render

def home_page(request):
	return render(request, 'FITLTS/home.html', {})
	
def calculator_page(request):
	return render(request, 'FITLTS/calculator/new.html', {})
	
def alg_overview_page(request):
	return render(request, 'FITLTS/alg_overview.html', {})
	
def library_page(request):
	return render(request, 'FITLTS/cpplibrary.html', {})