from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
from django.contrib import admin

import LTSFactories
import views

# load on startup
admin.autodiscover()
LTSFactories.setup_module() 

urlpatterns = patterns('',
	url(r'^index/$', views.home_page, name='home_page'),
	url(r'^calculator/$', views.calculator_page, name='calculator_page'),
	url(r'^calculator/', include('LTSTasks.urls', namespace='ltstasks')),
	url(r'^overview/$', views.alg_overview_page, name='alg_overview_page'),
	url(r'^library/$', views.library_page, name='library_page'),	
	url(r'^summary/', include('LTSSummary.urls', namespace='ltssummary')),
#	url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
	url(r'^admin/', include(admin.site.urls)),
	url(r'^/?$', RedirectView.as_view(url=reverse_lazy('home_page'))),
)