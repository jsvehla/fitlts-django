from django.conf import settings

from models import AlgDetailsFactory
from models import ObjectChoiceFactory

ALG_FACTORY = AlgDetailsFactory()
TRANS_FACTORY = ObjectChoiceFactory()

def setup_module():

	algorithms = settings.ALGORITHM_CLS
	map(load_alg, algorithms)
	
	trans = settings.TRANSFORMATION_CLS
	transcls = map(cls_load, trans)
	for t in transcls:
		TRANS_FACTORY.register_class(t)
		
	print ALG_FACTORY
	print TRANS_FACTORY
		
def load_alg(tup):
	global ALG_FACTORY
	
	alg = cls_load(tup[0])
	form = cls_load(tup[1])
	ALG_FACTORY.register_class(alg, form)	

def cls_load(name):
	class_data = name.split(".")
	module_path = ".".join(class_data[:-1])
	class_str = class_data[-1]
	module = __import__(module_path, fromlist=[class_str])
	return getattr(module, class_str)
	