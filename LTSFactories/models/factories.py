class ObjectChoiceFactory(object):
	"""Class responsible for translation between string and object instances."""
	def __init__(self):
		self.classes = {}
		self.choices = []
	
	def register_class(self, cls):
		"""Registers class using its name and provides the choice selection for Django forms. The name for choice is taken using get_long_name method of instance."""
		
		self.classes[cls.__name__] = cls
		self.choices.append((cls.__name__, cls.get_long_name()))

	def get_choices(self):
		return self.choices
		
	def get_object(self, class_name, *args, **kwargs):
		"""Creates object from class name and passed parameters."""
		return self.classes[class_name](*args, **kwargs)
		
	def get_obj_name(self, obj):
		return obj.__class__.__name__
		
class DBChoiceFactory(ObjectChoiceFactory):
	"""Class responsible for translation between string enums and object instances.""" 
	
	def register_class(self, cls):
		"""Registers class using short name from get_char_name and provides the choice selection for Django. The name for choice is taken using get_long_name method of instance."""
		name = cls.get_char_name()
		self.classes[name] = cls
		self.choices.append((name, cls.get_long_name()))
	
	def get_object(self, char_name, *args, **kwargs):
		"""Creates object from class name and passed parameters."""
		return self.classes[char_name](*args, **kwargs)
		
	def get_obj_name(self, obj):
		return obj.get_char_name()
		
class AlgDetailsFactory(object):
	"""Class responsible for translation between string and object instances. Also provide functionality to register algorithm to forms."""
	def __init__(self):
		self.forms = {}
		self.choices_factory = ObjectChoiceFactory()
	
	def register_class(self, cls, form):
		"""Registers class using its name and associated form. The name for choice is taken using get_long_name method of instance."""
		self.choices_factory.register_class(cls)
		self.forms[cls.__name__] = form
		
	def get_choices(self):
		return self.choices_factory.get_choices()
		
	def get_object(self, class_name, *args, **kwargs):
		"""Creates object from class name and passed parameters."""
		return self.choices_factory.get_object(class_name, *args, **kwargs)
		
	def get_obj_name(self, obj):
		return self.choices_factory.get_obj_name(obj)
		
	def get_form(self, class_name):
		"""Gets associated form for class."""
		return self.forms[class_name]
	
	def get_forms(self):
		return self.forms
