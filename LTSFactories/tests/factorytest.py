from django.test import TestCase

from LTSFactories.models import ObjectChoiceFactory

class FactoryTests(TestCase):	
	def testFactoryGetAlgorithm(self):
		factory = ObjectChoiceFactory()
		factory.register_class(LTSAlgorithmVA)
		factory.register_class(LTSAlgorithmBSA)
		factory.register_class(LTSAlgorithmBSAM)
		
		va = factory.get_object(LTSAlgorithmVA.__name__, cycles=10, kParam = 5, hParam = 2)
		bsa = factory.get_object(LTSAlgorithmBSA.__name__, hParam = 10)
		bsam = factory.get_object(LTSAlgorithmBSAM.__name__, hParam = 10, lowHParam = 12)
		
		self.assertEqual(va.cycles,10)
		self.assertEqual(va.kParam,5)
		self.assertEqual(va.hParam,2)
		self.assertEqual(bsa.hParam,10)
		self.assertEqual(bsam.hParam,10)
		self.assertEqual(bsam.lowHParam,12)