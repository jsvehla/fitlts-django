from celery.task import Task
from celery.registry import tasks
from celery.utils.log import get_task_logger

from templated_email import get_templated_mail

from LTSTasks.models import LTSTask

logger = get_task_logger(__name__)

class ComputeTask(Task):
	def run(self, taskid, **kwargs):
		logger.info("Task %s running" % (taskid))
		task = LTSTask.objects.get(id=taskid)
		return task.compute()
		
class MailTask(Task):
	def run(self, recipient, group_url, **kwargs):
		logger.info("Sending mail %s" % (group_url))
		msg = get_templated_mail(
			template_name='job_queued',
			to=[recipient],
			context={
				'url': group_url,
			},
		)
		msg.send()		

tasks.register(ComputeTask)
tasks.register(MailTask)