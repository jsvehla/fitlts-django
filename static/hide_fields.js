window.onload=function(){
	var inputs = document.getElementsByTagName("select");
	for (var i = 0; i < inputs.length; i++) {
		if(inputs[i].id.indexOf("column_") != -1) {
			// add onChanged handler
			inputs[i].onchange = function(id){
				return function(){ input_changed(id); };
			}(inputs[i].id);
			// run it at first view to set fields accordingly
			input_changed(inputs[i].id);
		}
	}
}

function input_changed(id) {
	var sel = document.getElementById(id);
	var val = sel.options[sel.selectedIndex].value;
	var wrapper = document.getElementById(id + "_param").parentNode;

	// toggle class	
	if( val == "LTSTransNone" || val == "LTSTransExclude") {	
		wrapper.className += " hidden";
	} else {
		wrapper.className = wrapper.className.replace( /(?:^|\s)hidden(?!\S)/g , '' );
	}
}