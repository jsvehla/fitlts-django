from django import forms

class WizardStep(object):
    """Encapsulates form class and handles direct requests to the form."""
    def __init__(self, step_id, form):
        """Constructor.
        
        step_id -- name
        form    -- Django.Form class associated with step
        """
        if not issubclass(form, (forms.Form, forms.ModelForm,)):
            raise ValueError('Form must be subclass of a Django Form')

        self.id = str(step_id)
        self.form = form

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        if isinstance(other, WizardStep):
            return self.__hash__() == other.__hash__()
        return False

    def __ne__(self, other):
        return not self == other
 
    def __unicode__(self):
        return self.id
    
    def __str__(self):
        return self.id

    def get_template(self, request):
        """Returns path to the associated template.
        
        request -- HTTPRequest
        
        returns: string
        """
        NotImplementedError('Template not specified.')
        
    def initialize_form(self, request, session, form_data, *args, **kwargs):
        """Initializes form instance from class.
        
        request     -- HTTPRequest
        session     -- session data. Allows for passing data between forms
        form_data   -- initial data
        
        returns: Form instance
        """    
        return self.form(data=form_data)
        
    def pre_render_form(self, request, session, form_inst):
        """Returns additional context for the template
        
        request     -- HTTPRequest
        session     -- session data
        form_inst   -- form instance
        
        returns: Dict
        """
        return None
        
    def post_to_form(self, request,  session, *args, **kwargs):
        """Handles form submit.
        
        request -- HTTPRequest
        session -- session storage
        
        returns: Form
        """        
        return self.form(data=request.POST, files=request.FILES)
        