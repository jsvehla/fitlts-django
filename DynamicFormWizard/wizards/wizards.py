from functools import wraps
import urlparse
import sys
from django.utils.decorators import method_decorator
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponseNotAllowed
from django.http import HttpResponseNotFound

from wizardstate import WizardState
from wizardstep import WizardStep
from exceptions import UnknownStepException

class DynamicWizard(object):
	"""Dynamic wizard class handles temporary data storage and transition between forms"""
	
	def __init__(self):
		clazz = self.__class__
		self.id = '%s.%s' % (clazz.__module__, clazz.__name__,)
		
	def _get_state(self, request):
		"""Returns a temporary storage from session.
		
		request -- HTTPRequest
		
		returns: WizardState
		"""
		return request.session[self.id]
	
	def _render_form(self, request, step, form_inst):
		"""Adds an additional context to the template and calls a render_form method.
		
		request -- HTTPRequest
		step -- current step
		form -- current form instance
		
		returns: HTTPResponse
		"""
		add_context = step.pre_render_form(request, self._get_state(request), form_inst)
		return self.render_form(request, step, form_inst, {
									'cancel_step' : self.get_cancel_step(),
									'current_step': step,
									'next_step': self.get_next_step(request, step),
									'previous_step': self.get_previous_step(request, step),
									'form': form_inst,
									'context': add_context
									})
	
	def _get_step_offset(self, request, current_step, offset):
		"""Get step
		
		request -- HTTPRequest
		current_step -- current step
		offset -- index of step to get from current
		
		returns: WizardStep
		"""
		steps = self.get_steps_list(request)
		nindex = steps.index(current_step) + offset
		if nindex < 0 or nindex >= len(steps):
			return None
		return steps[nindex]
		
	def init_session(self, request, *args, **kwargs):
		""" initializes session storage
		
		request -- HTTPRequest
		"""
		if self.id not in request.session:
			request.session[self.id] = WizardState(form_data={})
		
	def handle_request(self, request, *args, **kwargs):
		"""Handles all requests. Depending on state/method type, will call other handle_*.
		
		request -- HTTPRequest
		
		returns: HTTPResponse or HTTPResponseNotAllowed
		
		"""
		self.init_session(request, *args, **kwargs)
		self.initialize(request, request.session[self.id], *args, **kwargs)
		
		step_id = kwargs.get('step_id', self.get_steps_list(request)[0].id) # move to current or beginning
		step = self.get_step(request, step_id)		

		if not step:
			if step_id == self.get_cancel_step():
				return self.handle_CANCEL(request, *args, **kwargs)
			return HttpResponseNotFound("Unknown step: " + str(step_id))

		if request.method == 'GET':
			return self.handle_GET(request, step, *args, **kwargs)
		elif request.method == 'POST':
			return self.handle_POST(request, step, *args, **kwargs)
		else:
			return HttpResponseNotAllowed(['GET', 'POST'])
	
	def handle_KEYERROR(self, request, exception):
		raise
		
	def handle_GET(self, request, step, *args, **kwargs):
		"""Handles GET method --- view form.
		
		request -- HTTPRequest
		step -- step name
		
		returns: HTTPResponse
		"""		
		form_data = self.get_form_data(request, step)
		try:
			form = step.initialize_form(request, self._get_state(request), form_data, *args, **kwargs)
		except KeyError as ex:
			return self.handle_KEYERROR(request, ex)
		
		return self._render_form(request, step, form)
	
	def handle_POST(self, request, step, *args, **kwargs):
		"""Handles POST --- submit.
		
		request -- HTTPRequest
		step -- step instance
		
		returns: HTTPResponse
		"""
		
		form = step.post_to_form(request, self._get_state(request), *args, **kwargs)
	
		if not form.is_valid():
			return self._render_form(request, step, form)
		
		self.pre_save_form(request, step, form, *args, **kwargs)
		self.save_form_data(request, step, form)
		self.post_save_form(request, step, form, *args, **kwargs)
		
		next_step = self.get_next_step(request, step)
		if next_step:
			return HttpResponseRedirect(urlparse.urljoin(self.get_base_url(request, step.id), next_step.id))
		return self.handle_DONE(request)		
	
	def get_form_data(self, request, step):
		"""Reads associated form data from storage.
		
		request -- HTTPRequest
		step -- step instance
		
		returns: Dict
		"""
		return self._get_state(request).form_data.get(step.id, None)
	
	def save_form_data(self, request, step, form):
		"""Saves form data to session.
		
		request -- HTTPRequest
		step -- step instance
		form -- form instance
		"""		
		self._get_state(request).form_data[step.id] = form.cleaned_data
		request.session.modified = True		#set True to save session
		
	def get_step(self, request, step_id):
		"""Gets step by name.
		
		request -- HTTPRequest
		
		returns: WizardStep
		"""
		steps = self.get_steps_list(request)
		try:
			return [step for step in steps if step.id == step_id][0]
		except IndexError:
			return None
			
	def get_next_step(self, request, step):
		"""Returns next step.
		
		request -- HTTPRequest
		step -- current step instance
		
		returns: WizardStep
		"""		
		return self._get_step_offset(request, step, 1)
	
	def get_previous_step(self, request, step):
		"""Returns previous step.
		
		request -- HTTPRequest
		step -- current step instance
		
		returns: WizardStep
		"""	
		return self._get_step_offset(request, step, -1)
		
	def get_base_url(self, request, step_id):
		"""Gets base wizard url.
		
		request -- HTTPRequest
		step_id -- current step name
		
		returns: String
		"""
		baseix = request.path.rfind(step_id)
		return request.path[:baseix]
		
	def render_form(self, request, step, form_inst, context):
		"""Returns rendered form
		
		request -- HTTPRequest
		step	-- step instance
		form	-- form instnce
		context -- additional context
		
		returns: HTTPResponse
		"""
		return render_to_response(step.get_template(request), context, RequestContext(request))

	def get_cancel_step(self):
		"""Cancel step name.
		returns: String
		"""
		return 'cancel'	
	
	def cleanup_session(self, request):
		"""Deletes session data"""
		del request.session[self.id]
		
	def handle_CANCEL(self, request, *args, **kwargs):
		"""Cleanups session data and redirects to base url.
		
		request -- HTTPRequest
		
		returns: HTTPResponse
		"""
		self.cleanup_session(request)
		return HttpResponseRedirect(self.get_base_url(request, self.get_cancel_step()))
		
	def initialize(self, request, state, *args, **kwargs):
		pass
		
	def pre_save_form(request, step, form, *args, **kwargs):
		pass
	
	def post_save_form(request, step, form, *args, **kwargs):
		pass
	
	def get_steps_list(self, request):
		""" Returns list of steps.
		
		request -- HTTPRequest
		
		returns: List
		"""
		NotImplementedError("Override to implement wizard")
		
	def handle_DONE(self, request):
		"""After the last step is finished, this metod is called to operate on temporary data.
		
		request -- HTTPRequest
		
		returns: HTTPResponse
		"""
	
		NotImplementedError("Override to implement wizard")