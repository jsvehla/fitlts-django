from UserDict import UserDict

class WizardState(UserDict):
	"""Wizard data storage. forms data are stored in self.form_data."""
	def __init__(self, *args, **kwargs):
		UserDict.__init__(self, *args, **kwargs)

		self.form_data = kwargs.get('form_data', {})