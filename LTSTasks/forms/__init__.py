from forms import MatrixDetailsForm
from forms import UserDetailsForm
from forms import AlgorithmForm
from forms import AlgorithmVADetailsForm
from forms import AlgorithmBSADetailsForm
from forms import AlgorithmBSAMDetailsForm
from forms import ConfirmForm
from forms import MailDetailsForm