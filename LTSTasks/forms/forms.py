from django import forms

from LTSFactories import ALG_FACTORY
from LTSFactories import TRANS_FACTORY

import LTSAlgorithms.models as m

class UserDetailsForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'

	email = forms.EmailField(label='Email Address')
	docfile = forms.FileField(label='Matrix file')
	
	def parse_matrixfile(self, f):
		try:
			array = []
			for line in f: # read lines
				tmp = [float(x) for x in line.split()]
				tmp.insert(0, 1.0)	# insert intercept column
				array.append(tmp)
			self.validate_matrix(array)
		except ValueError:
			raise forms.ValidationError('File has invalid format')
		return array
		
	def validate_matrix(self, matarr):
		w = len(matarr[0])
		h = len(matarr)
		
		for line in matarr:
			if len(line) != w:
				raise forms.ValidationError('Invalid format. Lenght of rows is not constant.')
		return True
		
	def clean_docfile(self):
		data = self.cleaned_data['docfile']
		parsed = self.parse_matrixfile(data)
		self.cleaned_data['docfile_parsed'] = parsed
		return data
		
class MailDetailsForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'

	email = forms.EmailField(label='Email Address')
	
	def __init__(self, matrix, *args, **kwargs):
		self.matrix = matrix
		super(MailDetailsForm, self).__init__( *args, **kwargs)
	
	def clean(self):
		self.cleaned_data['docfile_parsed'] = self.matrix
		return self.cleaned_data
	
class MatrixDetailsForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'
	
	def __init__(self, count, *args, **kwargs):
		super(MatrixDetailsForm, self).__init__( *args, **kwargs)
		self.count = count
		
		for i in range(0, count):
			self.fields['column_{index}'.format(index=i)] = forms.ChoiceField(label='Select column {index} transformation'.format(index=i), choices=TRANS_FACTORY.get_choices())
			self.fields['column_{index}_param'.format(index=i)] = forms.IntegerField(label='Parameter', required=False)
	
	def clean(self):
		cleaned_data = super(MatrixDetailsForm, self).clean()
				
		for i in range(0, self.count):
			trans = self.cleaned_data['column_{index}'.format(index=i)]
			param = self.cleaned_data['column_{index}_param'.format(index=i)]
			
			# call validation method by name
			try:
				self.cleaned_data['column_{index}_param_dict'.format(index=i)] = getattr(self, 'clean_param_' + trans)(param)
			except forms.ValidationError as ex:
				self._errors['column_{index}_param'.format(index=i)] = self.error_class(ex.messages)
			
		return cleaned_data
	
	def clean_param_LTSTransNone(self, param):
		return {}
	
	def clean_param_LTSTransMultiply(self, param):
		if param == None:
			raise forms.ValidationError('Parameter is required.')
		return {'parameter' : param }
		
	def clean_param_LTSTransLog(self, param):
		if param == None:
			raise forms.ValidationError('Parameter is required.')
		return {'base' : param }
	
	def clean_param_LTSTransPower(self, param):
		if param == None:
			raise forms.ValidationError('Parameter is required.')
		if param <= 0:			
			raise forms.ValidationError('Parameter must be positive integer.')
		return {'parameter' : param }
		
	def clean_param_LTSTransExclude(self, param):
		return {}
			
class AlgorithmForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'
	
	def __init__(self, count, *args, **kwargs):
		super(AlgorithmForm, self).__init__( *args, **kwargs)
		self.count = count
		
		ALG_CHOICES = [('None', 'None')] + ALG_FACTORY.get_choices()

		self.fields['algorithm_0'] = forms.ChoiceField(label='Select algorithm', choices=ALG_FACTORY.get_choices())
		for i in range(1, self.count):
			self.fields['algorithm_{index}'.format(index=i)] = forms.ChoiceField(label='Select algorithm', choices=ALG_CHOICES)

class AlgorithmDetailsForm(forms.ModelForm):
	@classmethod
	def get_alg_name(cls):
		return cls.Meta.model.__name__
			
class AlgorithmVADetailsForm(AlgorithmDetailsForm):
	error_css_class = 'error'
	required_css_class = 'required'

	def __init__(self, matrix, *args, **kwargs):
		self.matrix = matrix
		super(AlgorithmVADetailsForm, self).__init__( *args, **kwargs)
	
	def clean_hParam(self):
		hParam = self.cleaned_data['hParam']
		if hParam < 0 or hParam > len(self.matrix):
			raise forms.ValidationError('Parameter H must be positive integer less that %s' % len(self.matrix))
		return hParam
	
	def clean_kParam(self):
		kParam = self.cleaned_data['kParam']
		if kParam < 0 or kParam > len(self.matrix):
			raise forms.ValidationError('Parameter K must be positive integer less that %s' % len(self.matrix))
		return kParam
	
	def clean_cycles(self):
		cycles = self.cleaned_data['cycles']
		if cycles < 0:
			raise forms.ValidationError('Parameter cycles must be positive integer')
		return cycles

	class Meta:
		model = m.LTSAlgorithmVA
		
class AlgorithmBSADetailsForm(AlgorithmDetailsForm):
	error_css_class = 'error'
	required_css_class = 'required'
	
	def __init__(self, matrix, *args, **kwargs):
		self.matrix = matrix
		super(AlgorithmBSADetailsForm, self).__init__( *args, **kwargs)
	
	def clean_hParam(self):
		hParam = self.cleaned_data['hParam']
		if hParam < 0 or hParam > len(self.matrix):
			raise forms.ValidationError('Parameter H must be positive integer less that %s' % len(self.matrix))
		return hParam

	class Meta:
		model = m.LTSAlgorithmBSA
		
class AlgorithmBSAMDetailsForm(AlgorithmDetailsForm):
	error_css_class = 'error'
	required_css_class = 'required'
	
	def __init__(self, matrix, *args, **kwargs):
		super(AlgorithmBSAMDetailsForm, self).__init__( *args, **kwargs)
		self.fields['hParam'].label = "Parameter H (excluded)"
		self.matrix = matrix	
	
	def clean_hParam(self):
		hParam = self.cleaned_data['hParam']
		if hParam < 0 or hParam > len(self.matrix):
			raise forms.ValidationError('Parameter H must be positive integer less that %s' % len(self.matrix))
		return hParam
	
	def clean_lowHParam(self):
		try:
			hParam = self.cleaned_data['hParam']
		except KeyError:
			raise forms.ValidationError('Parameter H is invalid')
			
		lhParam = self.cleaned_data['lowHParam']
		if lhParam < 0 or lhParam >= hParam:
			raise forms.ValidationError('Parameter low H must be positive integer less that %s' % hParam)
		return lhParam

	class Meta:
		model = m.LTSAlgorithmBSAM
		
class ConfirmForm(forms.Form):
	pass