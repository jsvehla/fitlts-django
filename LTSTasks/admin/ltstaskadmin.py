from django.contrib import admin
from django.core import urlresolvers
from django.contrib.contenttypes.models import ContentType

from LTSAlgorithms.admin import CustomAdminBase
from LTSTasks.models.ltstask import LTSTask
from LTSTasks.models.ltstask import LTSTaskGroup

class LTSTaskAdmin(CustomAdminBase):
	list_display = CustomAdminBase.list_display + ( 'related_group_id', 'date_created', 'date_started', 'date_finished', 'state', 'related_matrix_id', 'related_algorithm_id' )
	readonly_fields = ( 'group', 'date_created', 'date_started', 'date_finished', 'state', 'matrix', 'algorithm' )
	list_filter =( 'group', 'state_str')
	
	def related_group_id(self, obj):
		obj = obj.group
		id = obj.uuid
		
		content_type = ContentType.objects.get_for_model(obj.__class__)
		url = urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(id,))
		text = unicode(id)
		return u"<a href='%s'>%s</a>" % (url, text)
	related_group_id.allow_tags = True
	
	def related_algorithm_id(self, obj):
		return self.get_id_text(obj.algorithm)
	related_algorithm_id.allow_tags = True
	
	def related_matrix_id(self, obj):
		return self.get_id_text(obj.matrix)
	related_matrix_id.allow_tags = True

class LTSTaskGroupAdmin(CustomAdminBase):
	list_display = ('uuid', )
	
	
admin.site.register(LTSTask, LTSTaskAdmin)
admin.site.register(LTSTaskGroup, LTSTaskGroupAdmin)
