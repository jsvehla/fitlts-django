from wizard import WizardNew
from wizard import WizardEdit
from wizardsteps import WizardStepEditStart
from wizardsteps import WizardStepAlgorithm
from wizardsteps import WizardStepAlgorithmDetails
from wizardsteps import WizardStepMatrixDetail
from wizardsteps import WizardStepUserDetail
from wizardsteps import WizardStepConfirm