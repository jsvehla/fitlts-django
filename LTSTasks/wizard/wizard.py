from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.forms import model_to_dict

from django.conf import settings

from DynamicFormWizard import wizards

from QueueCelery.models import ComputeTask
from QueueCelery.models import MailTask

from LTSTasks import forms
from LTSTasks import models as ltsm

from wizardsteps import WizardStepUserDetail
from wizardsteps import WizardStepMatrixDetail
from wizardsteps import WizardStepAlgorithm
from wizardsteps import WizardStepAlgorithmDetails
from wizardsteps import WizardStepConfirm
from wizardsteps import WizardStepEditStart

from LTSFactories import TRANS_FACTORY
from LTSFactories import ALG_FACTORY
from LTSAlgorithms.models import TransFormParamVisitor

class WizardNew(wizards.DynamicWizard):
	def get_steps_list(self, request):
		steps = [
			WizardStepUserDetail(),
			WizardStepMatrixDetail(),
			WizardStepAlgorithm(settings.MAX_ALGORITHMS_PER_GROUP),
		]
		
		if 'algorithm' in self._get_state(request).form_data:
			alg_data = self._get_state(request).form_data['algorithm']
			index = 0
			for key in sorted(alg_data.iterkeys()):
				if alg_data[key] != 'None':
					steps.append(WizardStepAlgorithmDetails(alg_data[key], index))
					index += 1
		steps.append(WizardStepConfirm(self))
		return steps
	
	def handle_CANCEL(self, request, *args, **kwargs):
		super(WizardNew, self).handle_CANCEL(request, *args, **kwargs)
		return HttpResponseRedirect(reverse('calculator_page'))
		
	def parse_data(self, request):
		storage = request.session[self.id]
		
		# matrix
		mat = storage.form_data['start']['docfile_parsed']
		mail = storage.form_data['start']['email']
		
		# transformations array, details
		trans_dict = storage.form_data['matrix']
		trans_arr = []
		transdetail_arr = []
		for i in range(0, len(mat[0])):
			trans_arr.append(trans_dict['column_{index}'.format(index=i)])
			transdetail_arr.append(trans_dict['column_{index}_param_dict'.format(index=i)])
		
		# algorithms array
		alg_arr = []
		alg_data = storage.form_data['algorithm']
		for key in sorted(alg_data.iterkeys()):
				if alg_data[key] != 'None':
					alg_arr.append(alg_data[key])
		
		# algorithms details
		algdetail_arr = []
		for i in range(len(alg_arr)):
				algdetail_arr.append(storage.form_data['detail' + str(i)])
		
		return {"mail":mail,"matrix":mat, "transformations": trans_arr, "trans_details": transdetail_arr, "algorithms": alg_arr, "alg_details": algdetail_arr}	

	def handle_DONE(self, request):
		parsed = self.parse_data(request)
		group = ltsm.LTSTaskGroup.create(parsed['matrix'], parsed["transformations"], parsed["trans_details"], parsed["algorithms"], parsed["alg_details"])
		group_url = request.build_absolute_uri(reverse('ltssummary:summary', args=(group.uuid,)))
		
		self.send_email(parsed["mail"], group_url)
		for task in group.ltstask_set.all():
			self.enqueue_task(task)		
		
		last_step = self.get_steps_list(request)[-1]
		self.cleanup_session(request)
		
		return render(request, 'FITLTS/calculator/done.html', {"group_url": group_url })
	
	def send_email(self, recipient, group_url):
		MailTask.delay(recipient, group_url)
	
	def enqueue_task(self, task):
		qres = ComputeTask.delay(task.id)
		task.celery_task_id = qres.id
		task.save()
		
	def handle_KEYERROR(self, request, exception):
		return render(request, 'LTSTasks/wizard_keyerror.html', {})
		
class WizardEdit(WizardNew):
	def prepare_data(self, request, group):
		self.init_session(request)
		storage = self._get_state(request)
		
		tasks = group.ltstask_set.all()
		
		# matrix
		storage.form_data['start'] = {}
		
		matrix = tasks[0].matrix
		storage.form_data['start']['email'] = ''
		storage.form_data['start']['docfile_parsed'] = matrix.get_matrix()
		
		# transformations
		storage.form_data['matrix'] = {}
		trans = matrix.ltstransformation_set.all()
		for i in range(0, len(trans)):
			storage.form_data['matrix']['column_{index}'.format(index=i)] = TRANS_FACTORY.get_obj_name(trans[i])
			
			tvis = TransFormParamVisitor()
			trans[i].accept(tvis)
			storage.form_data['matrix']['column_{index}_param'.format(index=i)] = tvis.parameter
		
		# algorithms
		storage.form_data['algorithm'] = {}
		for i in range(0, settings.MAX_ALGORITHMS_PER_GROUP):
			if i >= len(tasks):
				storage.form_data['algorithm']['algorithm_{index}'.format(index=i)] = 'None'
			else:
				alg = tasks[i].algorithm.as_leaf_class()
				storage.form_data['algorithm']['algorithm_{index}'.format(index=i)] = ALG_FACTORY.get_obj_name(alg)
				storage.form_data['detail{index}'.format(index=i)] = model_to_dict(alg)
		
		request.session.modified = True
		print storage.form_data

	def get_steps_list(self, request):
		steps = [
			WizardStepEditStart(),
			WizardStepMatrixDetail(),
			WizardStepAlgorithm(settings.MAX_ALGORITHMS_PER_GROUP),
		]
		
		if 'algorithm' in self._get_state(request).form_data:
			alg_data = self._get_state(request).form_data['algorithm']
			index = 0
			for key in sorted(alg_data.iterkeys()):
				if alg_data[key] != 'None':
					steps.append(WizardStepAlgorithmDetails(alg_data[key], index))
					index += 1
		steps.append(WizardStepConfirm(self))
		return steps
	