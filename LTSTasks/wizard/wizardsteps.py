from DynamicFormWizard import wizards
from LTSTasks import forms
from LTSFactories import ALG_FACTORY
from LTSFactories import TRANS_FACTORY

from LTSAlgorithms.models import ParamVisitor
from LTSAlgorithms.models import TransParamVisitor

class WizardStepUserDetail(wizards.WizardStep):
	def __init__(self):
		super(WizardStepUserDetail,self).__init__('start', forms.UserDetailsForm)
		
	def get_template(self, request):
		return 'LTSTasks/wizard_start.html'
	
	def pre_render_form(self, request, session, form_inst):
		return {"step_num" : 1, "step_total" : 5}
		
class WizardStepEditStart(wizards.WizardStep):
	def __init__(self):
		super(WizardStepEditStart,self).__init__('start', forms.MailDetailsForm)
		
	def get_template(self, request):
		return 'LTSTasks/wizard_start_edit.html'
		
	def initialize_form(self, request, session, form_data, *args, **kwargs):
		matrix = session['form_data']['start']['docfile_parsed']
		return self.form(data=form_data, matrix=matrix)
		
	def post_to_form(self, request, session, *args, **kwargs):
		matrix = session['form_data']['start']['docfile_parsed']
		return self.form(data=request.POST, files=request.FILES, matrix=matrix)
	
	def pre_render_form(self, request, session, form_inst):
		return {"step_num" : 1, "step_total" : 5}
		
class WizardStepMatrixDetail(wizards.WizardStep):
	def __init__(self):
		super(WizardStepMatrixDetail,self).__init__('matrix', forms.MatrixDetailsForm)
	
	def pre_render_form(self, request, session, form_inst):
		mat = session.form_data['start']['docfile_parsed']
		return {"matrix" : mat, "step_num" : 2, "step_total" : 5}
	
	def get_template(self, request):
		return 'LTSTasks/wizard_matrix.html'
		
	def initialize_form(self, request, session, form_data, *args, **kwargs):
		matrix = session['form_data']['start']['docfile_parsed']
		return self.form(data=form_data, count=len(matrix[0]))
	
	def post_to_form(self, request, session, *args, **kwargs):
		matrix = session['form_data']['start']['docfile_parsed']
		return self.form(data=request.POST, files=request.FILES, count=len(matrix[0]))

class WizardStepAlgorithm(wizards.WizardStep):
	def __init__(self, count):
		self.count = count
		super(WizardStepAlgorithm,self).__init__('algorithm', forms.AlgorithmForm)
		
	def get_template(self, request):
		return 'LTSTasks/wizard_algorithms.html'
		
	def pre_render_form(self, request, session, form_inst):
		return {"step_num" : 3, "step_total" : 5}
	
	def initialize_form(self, request, session, form_data, *args, **kwargs):
		return self.form(data=form_data, count = self.count)
	
	def post_to_form(self, request, session, *args, **kwargs):
		return self.form(data=request.POST, files=request.FILES, count = self.count)

class WizardStepAlgorithmDetails(wizards.WizardStep):
	def __init__(self, alg, index):
		self.index = index
		form = ALG_FACTORY.get_form(alg)
		super(WizardStepAlgorithmDetails,self).__init__('detail' + str(self.index), form)
	
	def initialize_form(self, request, session, form_data, *args, **kwargs):
		matrix = session['form_data']['start']['docfile_parsed']
		return self.form(data=form_data, matrix=matrix)
	
	def post_to_form(self, request, session, *args, **kwargs):
		matrix = session['form_data']['start']['docfile_parsed']
		return self.form(data=request.POST, files=request.FILES, matrix=matrix)
	
	def get_template(self, request):
		return 'LTSTasks/wizard_algdetails.html'
		
	def pre_render_form(self, request, session, form_inst):
		return {"step_num" : 4, "step_total" : 5, 'substep': self.index + 1, 'algorithm' : form_inst.get_alg_name() }
				
class WizardStepConfirm(wizards.WizardStep):
	def __init__(self, parent):
		self.parent = parent
		super(WizardStepConfirm,self).__init__('confirm', forms.ConfirmForm)
	
	def pre_render_form(self, request, session, form_inst):
		parsed = self.parent.parse_data(request)
		trans_arr = parsed["transformations"]
		trans_detail = parsed["trans_details"]
		mat = zip(*parsed["matrix"])
		alg_arr = parsed["algorithms"]
		algdetail_arr = parsed["alg_details"]
		
		mat_tr = []
		trans = []
		algs = []
		for i in range(len(trans_arr)):
			tmp = TRANS_FACTORY.get_object(trans_arr[i], **trans_detail[i])
			row = tmp.transform(mat[i])
			if row:
				mat_tr.append(row)
				
			tvis = TransParamVisitor()
			tmp.accept(tvis)
			trans.append({"name": tmp.get_long_name(), "params": tvis.params })
		
		for i in range(len(alg_arr)):
			tmp = {}
			atmp = ALG_FACTORY.get_object(alg_arr[i], **algdetail_arr[i])
			pvis = ParamVisitor()
			atmp.accept(pvis)
			algs.append({"algorithm": atmp.get_long_name(), "params": pvis.params})
							
		return {"matrix" : zip(*mat_tr), "transformations": trans, "algorithms": algs, "step_num" : 5, "step_total" : 5}
	
	def get_template(self, request):
		return 'LTSTasks/wizard_confirm.html'	
