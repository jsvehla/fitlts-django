from LTSTasks.wizard import WizardNew
from LTSTasks.wizard import WizardEdit

def wizard_new(request, step_id=None):
    return WizardNew().handle_request(request, step_id=step_id)
    
def wizard_edit(request, step_id=None):
    return WizardEdit().handle_request(request, step_id=step_id)
