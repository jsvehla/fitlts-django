from django.db import models
from django.utils import timezone
import numpy as np
import uuid
from django.db import transaction
import sys
from celery.task.control import revoke

from LTSTasks.models import TaskStateField
from LTSTasks.models import TaskStateCommited
from LTSTasks.models import TaskStateFailed
from LTSTasks.models import TASKSTATE_FACTORY
from LTSAlgorithms.models import LTSMatrix
from LTSAlgorithms.models import LTSAlgorithmBase
from LTSFactories import ALG_FACTORY
from LTSFactories import TRANS_FACTORY


def make_uuid():
    return str(uuid.uuid1())

class LTSTaskGroup(models.Model):
	"""Class that serves to hold multiple instances of LTSTask from single input data process.
	"""
	uuid = models.CharField(max_length=36, primary_key=True, default=make_uuid, editable=False)
	
	@staticmethod
	def create(matrix_arr, trans_arr, transdetail_arr, alg_arr, algdetails_arr):
		"""Creates new task group from provided data.
		
		matrix_arr:			Array
		trans_arr:			Array of transformation classnames
		transdetail_arr:	Array of dictionaries with named constructor parameters.
		alg_arr:			Array of algorithms classnames
		algdetails_arr:		Array of dictionaries with named costructor parameters.
		
		returns:	LTSTaskGroup
		"""
		with transaction.commit_on_success():
			taskGroup = LTSTaskGroup()
			taskGroup.save()
			
			mat = matrix_arr
			m = LTSMatrix()
			m.save() 
			m.fillArray(mat)
			
			for i in range(len(trans_arr)):
				trans = TRANS_FACTORY.get_object(trans_arr[i], **transdetail_arr[i])
				trans._matrix = m
				trans._col_num = i
				trans.save()
			m.save()
					
			for i in range(len(alg_arr)):
				task = LTSTask()
				task.date_created = timezone.now()
				task.matrix = m
		
				alg = ALG_FACTORY.get_object(alg_arr[i], **algdetails_arr[i])
				alg.save()
				task.algorithm = alg
				task.state = TaskStateCommited()
				task.group = taskGroup
				task.clean_fields()
				task.save()
			return taskGroup

	def __unicode__(self):
		return ("LTSTaskGroup uuid:" + unicode(self.uuid))

	class Meta:
		app_label = 'LTSTasks'
		verbose_name = 'LTSTaskGroup'
		verbose_name_plural = 'LTSTaskGroups'

class LTSTask(models.Model):
	"""Class responsible for holding single task data."""
	celery_task_id = models.CharField(max_length = 50, unique=True, blank=True, null=True)
	date_created = models.DateTimeField()
	date_started = models.DateTimeField(null=True, blank=True)
	date_finished = models.DateTimeField(null=True, blank=True)
	state = TaskStateField()
	
	matrix = models.ForeignKey(LTSMatrix)
	algorithm = models.OneToOneField(LTSAlgorithmBase)
	group = models.ForeignKey(LTSTaskGroup, null=True, blank=True)
	
	def _associateResults(self, result_list):
		"""Associates given result with this task
		"""
		for res in result_list:
			res.for_task = self
			res.save()
	
	def compute(self):
		"""Computes the lts estimate and associates it with self. Handles current state transitions.
		"""
		if self.state.ready(self) == True:
			try:
				self.state.doTransition(self)
				self.date_started = timezone.now()
				self.save()
				
				res = self.algorithm.as_leaf_class().solve(self.matrix.get_matrix_trans())
				self._associateResults(res)
				self.state.doTransition(self)
				self.date_finished = timezone.now()
				self.save()
				return res
			except Exception:
				self.state = TaskStateFailed()
				self.save()
				type, value, traceback = sys.exc_info()
				raise type, value, traceback
		return None
		
	def cancel(self):
		"""Cancels task and associated celery task. Attmpts to terminate if already running."""
		try:
			self.state.doCancel(self)
			revoke(self.celery_task_id, terminate=True)
			self.save()
		except ValueError:
			pass
		print self.state
					
	def __unicode__(self):
		return ("LTSTask id:" + unicode(self.id) + " state:" + unicode(self.state))

	class Meta:
		app_label = 'LTSTasks'
		verbose_name = 'LTSTask'
		verbose_name_plural = 'LTSTasks'		
		