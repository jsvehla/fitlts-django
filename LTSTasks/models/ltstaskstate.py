from django.db import models
from LTSFactories.models import DBChoiceFactory

class TaskState(object):
	"""Bas class for states."""
	@staticmethod
	def get_char_name():
		"""Gets name that will be saved to the database."""
		raise NotImplementedError

	@staticmethod
	def get_long_name():
		"""Gets name that will be shown to the user"""
		raise NotImplementedError
		
	def ready(self, context):
		"""Check if the task is ready to be computed."""
		raise NotImplementedError
		
	def doTransition(self, context):
		"""Does correctly transition from current to running state."""
		raise NotImplementedError
		
	def doCancel(self, context):
		"""Does correctly transition from current to cancelled state."""
		raise NotImplementedError
				
	def __unicode__(self):
		return self.get_long_name()
		
	class Meta:
		app_label = 'LTSTasks'

class TaskStateCommited(TaskState):
	@staticmethod
	def get_char_name():
		return 'CO'
		
	@staticmethod
	def get_long_name():
		return 'Commited'
		
	def ready(self, context):
		return True
	
	def doTransition(self, context):
		context.state = TaskStateRunning()
		
	def doCancel(self, context):
		context.state = TaskStateCancelled()
		
	class Meta:
		app_label = 'LTSTasks'

class TaskStateDone(TaskState):
	@staticmethod
	def get_char_name():
		return 'DO'
		
	@staticmethod
	def get_long_name():
		return 'Done'

	def ready(self, context):
		return False

	def doTransition(self, context):
		raise ValueError("Cannot do transition: Final state")
		
	def doCancel(self, context):
		raise ValueError("Cannot do transition: Final state")

	class Meta:
		app_label = 'LTSTasks'

class TaskStateRunning(TaskState):
	@staticmethod
	def get_char_name():
		return 'RU'

	@staticmethod
	def get_long_name():
		return 'Running'
		
	def ready(self, context):
		return False
	
	def doTransition(self, context):
		context.state = TaskStateDone()
		
	def doCancel(self, context):
		context.state = TaskStateCancelled()
		
	class Meta:
		app_label = 'LTSTasks'

class TaskStateCancelled(TaskState):
	@staticmethod
	def get_char_name():
		return 'CA'

	@staticmethod
	def get_long_name():
		return 'Cancelled'
	
	def ready(self, context):
		return False
		
	def doTransition(self, context):
		raise ValueError("Cannot do transition: Final state")
		
	def doCancel(self, context):
		pass
		
	class Meta:
		app_label = 'LTSTasks'
		
class TaskStateFailed(TaskState):
	@staticmethod
	def get_char_name():
		return 'FA'

	@staticmethod
	def get_long_name():
		return 'Failed'
	
	def ready(self, context):
		return False
	
	def doTransition(self, context):
		raise ValueError("Cannot do transition: Final state")
	
	def doCancel(self, context):
		raise ValueError("Cannot do transition: Final state")
		
	class Meta:
		app_label = 'LTSTasks'	

TASKSTATE_FACTORY = DBChoiceFactory()
TASKSTATE_FACTORY.register_class(TaskStateCancelled)
TASKSTATE_FACTORY.register_class(TaskStateRunning)
TASKSTATE_FACTORY.register_class(TaskStateDone)
TASKSTATE_FACTORY.register_class(TaskStateCommited)
TASKSTATE_FACTORY.register_class(TaskStateFailed)	
		
class TaskStateField(models.CharField):
	"""Custom field responsible for handling of loading and saving tasks to the DB."""
	def __init__(self, *args, **kwargs):
		kwargs['blank'] = False
		kwargs['max_length'] = 2
		kwargs['choices'] = TASKSTATE_FACTORY.get_choices()
		super(TaskStateField, self).__init__(*args, **kwargs)
		
	def contribute_to_class(self, cls, name):
		if self.db_column is None:
			self.db_column = name
		self.field_name = name + '_str'
		super(TaskStateField, self).contribute_to_class(cls, self.field_name)
		setattr(cls, name, property(self.get_data, self.set_data))

	def get_data(self, obj):
		return TASKSTATE_FACTORY.get_object(getattr(obj, self.field_name))
		
	def set_data(self, obj, data):
		setattr(obj, self.field_name, TASKSTATE_FACTORY.get_obj_name(data))

