from django.test import TestCase
from django.utils import timezone
import numpy as np

from LTSTasks.models import LTSTask
from LTSTasks.models import TaskStateCommited
from LTSAlgorithms.models.ltsalgorithms import LTSAlgorithmVA
from LTSAlgorithms.models.ltsmatrix import LTSMatrix
from LTSAlgorithms.models.ltsresult import LTSResult
from QueueCelery.models import ComputeTask

class QueueTests(TestCase):
	def testVAQueued(self):
		t = LTSTask()
		t.date_created = timezone.now()
		
		mat = [
		[1.,80.,27.,89.,42.],
		[1.,80.,27.,88.,37.],
		[1.,75.,25.,90.,37.],
		[1.,62.,24.,87.,28.],
		[1.,62.,22.,87.,18.],
		[1.,62.,23.,87.,18.],
		[1.,62.,24.,93.,19.],
		[1.,62.,24.,93.,20.],
		[1.,58.,23.,87.,15.],
		[1.,58.,18.,80.,14.],
		[1.,58.,18.,89.,14.],
		[1.,58.,17.,88.,13.],
		[1.,58.,18.,82.,11.],
		[1.,58.,19.,93.,12.],
		[1.,50.,18.,89., 8.],
		[1.,50.,18.,86., 7.],
		[1.,50.,19.,72., 8.],
		[1.,50.,19.,79., 8.],
		[1.,50.,20.,80., 9.],
		[1.,56.,20.,82.,15.],
		[1.,70.,20.,91.,15.]]
		m = LTSMatrix()
		m.save()
		m.fillArray(mat)
		m.save()
			
		va = LTSAlgorithmVA(kParam=len(mat[0]), hParam=12, cycles=5000)
		va.clean_fields()
		va.save()
		
		t.algorithm = va
		t.matrix = m
		t.state = TaskStateCommited()
		t.clean_fields()
		t.save()
		
		qres = ComputeTask.delay(t.id)

		# reload
		res = LTSResult.objects.get(id=qres.get()[0].id)
		self.assertTrue(np.allclose(res.getEstimate(), np.array([-35.2095, 0.746057, 0.337795, -0.0054919]), 0.0001))
		self.assertTrue(np.alltrue(res.getVector() ==  np.array([0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0])))
		self.assertAlmostEqual(res.getResidualSum(), 1.6371, delta=0.1)