from django.test import TestCase
from django.utils import timezone
import numpy as np

from LTSTasks.models import *
from LTSAlgorithms.models.ltsalgorithms import LTSAlgorithmVA, LTSAlgorithmBase
from LTSAlgorithms.models.ltsmatrix import LTSMatrix
from LTSAlgorithms.models.ltsresult import LTSResult

class TaskTests(TestCase):
	def testStateField(self):
		t = LTSTask()
		t.date_created = timezone.now()
		
		st = TaskStateRunning()
		t.state = st
		t.matrix = LTSMatrix.objects.create()
		t.algorithm = LTSAlgorithmVA.objects.create(hParam=1, kParam=1, cycles=1)

		t.save()
		task = LTSTask.objects.get(id=t.id)
		
		self.assertEqual(task.state_str, st.get_char_name())
		
	def testAlgorithmField(self):
		t = LTSTask()
		t.date_created = timezone.now()		
		t.matrix = LTSMatrix.objects.create()
		va = LTSAlgorithmVA.objects.create(hParam=1, kParam=1, cycles=1)
		t.algorithm = va
		t.save()
		
		t2 = LTSTask.objects.get(id=t.id)
		va2 = t2.algorithm.as_leaf_class()
		
		self.assertEqual(va.__class__, va2.__class__)
	