from django.conf.urls import patterns, url
from django.views.generic import RedirectView

from LTSTasks import views

urlpatterns = patterns('',
	url(r'^new/(?P<step_id>[A-Za-z0-9_-]+)$', views.wizard_new, name='wizard_new'),
	url(r'^edit/(?P<step_id>[A-Za-z0-9_-]+)$', views.wizard_edit, name='wizard_edit'),
)